---
title: Mexicaanse bruschetta's
footer: © 2019 Wim Vandevenne
---

# Mexicaanse bruschetta's

<!-- ![hero.png](../../public/hero.png) -->

## Ingrediënten

- Brood
- Olijfolie
- Knoflook
- Gemalen kaas
- Salsa
- Guacamole
- Jalapenos

## Bereiding

- Sprenkel olijfolie en kruiden over het brood
- Rooster het brood in de oven
- Wrijf het brood in met knoflook
- Beleg het brood meteen met gemalen kaas, salsa, guacamole en jalapenos

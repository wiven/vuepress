module.exports = {
    title: 'Recepten WimLes',
    description: 'Recepten voor en door Wim & Lesley',
    base: '/vuepress/',
    dest: 'public',
    permalink: '/:regular',
    themeConfig: {
        nav: [{
                text: 'Home',
                link: '/'
            },
            {
                text: 'Aperitief',
                link: '/aperitief/'
            },
            {
                text: 'Hoofdgerecht',
                link: '/hoofdgerecht/'
            },
            {
                text: 'Dessert',
                link: '/dessert/'
            }
        ]
    }
}
---
title: Appelcaketaart
---

# Appelcaketaart <small>[🔗](https://www.colruyt.be/nl/recept/appelcaketaart)</small>

<img :src="$withBase('/appelcaketaart.jpg')" class="recipe-image" alt="Appelcaketaart">

## Ingrediënten

- Appelmoes
- 125 g boter
- 2 eieren
- 1 rol bladerdeeg (of kruimeldeeg)
- 3 eetl. abrikozenconfituur
- 125 g suiker
- 125 g zelfrijzende bloem

## Bereiding

**Bereiding: 15 min. + ± 30 min. in de oven**

- Scheid de eidooiers van de eiwitten.
- Verwarm de oven voor op 200 °C.
- Smelt 125 g boter in een kookpot. Klop er de suiker, de bloem en de eidooiers onder.
- Klop de eiwitten heel stijf en meng ze voorzichtig onder het deeg.
- Vet een taartvorm (Ø 25 cm) in met 1 koffiel. boter. Leg er het taartdeeg (zonder het bakpapier) in en prik er enkele keren in met een vork.
- Verdeel de appelmoes over de taartbodem en bedek met het cakedeeg.
- Schil 1 appel en verdeel hem in 8 partjes. Maak in elk partje 4inkepingen. Versier het deeg met de partjes.
- Zet de taart ± 30 min. in de voorverwarmde oven. De taart is klaar als u er met een mes in steekt en het er droog uitkomt.
- Verwarm intussen de abrikozenconfituur.

## Afwerking

Schuif de taart op een rooster en bestrijk ze met de abrikozenconfituur.
